from typing import List, Tuple
from numpy import linalg

import sklearn.naive_bayes
import sklearn
import sklearn.svm

import os
import pickle

import collections
import math

import csv
import requests
import numpy as np

SPAMBASE_FILE = "./spambase.pickle"


def angular_kernel(X, Y):
    product_matrix = np.dot(X, Y.T)
    shape = product_matrix.shape

    for row_index in range(0, shape[0]):
        product_matrix[row_index] /= linalg.norm(X[row_index])

    for column_index in range(0, shape[1]):
        product_matrix[:, column_index] /= linalg.norm(Y[column_index])

    product_matrix = -product_matrix
    # To remove the errors of floating point approximation
    product_matrix = np.around(product_matrix, decimals=7)
    product_matrix = np.arccos(product_matrix)

    return product_matrix


MODEL_MAP = {
    "angular": sklearn.svm.SVC(kernel=angular_kernel),
    "linear": sklearn.svm.SVC(C=30, kernel="linear", gamma="auto"),
    "poly": sklearn.svm.SVC(C=30, kernel="poly", gamma="auto", degree=2),
    "rbf": sklearn.svm.SVC(C=300, kernel="rbf", gamma="auto"),
    "gaussian": sklearn.naive_bayes.GaussianNB(),
}


def get_data() -> Tuple[List[List[float]], List[int]]:
    training_vectors = []
    target_values = []

    if os.path.exists(SPAMBASE_FILE):

        with open(SPAMBASE_FILE, "br") as f:
            data = pickle.load(f)
    else:
        with open(SPAMBASE_FILE, "bw") as f:
            data = requests.get(
                "https://archive.ics.uci.edu/ml/machine-learning-databases/spambase/spambase.data"
            )
            pickle.dump(data, f)

    mails = csv.reader(data.text.splitlines(), delimiter=",")
    for mail in mails:
        features_vector = list(map(float, mail[:54]))
        spam_feature = int(mail[57])

        if sum(features_vector) != 0:
            training_vectors.append(features_vector)
            target_values.append(spam_feature)

    return training_vectors, target_values


def tf_idf_converter(mails: List[List[float]]) -> List[List[float]]:
    document_freq = collections.Counter()  # init to 0

    for mail in mails:
        for index, feature in enumerate(mail):
            if feature > 0:
                document_freq[index] += 1

    tf_idf_matrix = []
    for mail in mails:
        tf_idf_row = []
        for index, feature in enumerate(mail):
            tf = feature / 100
            idf = math.log(len(mails) / document_freq[index])

            tf_idf_row.append(tf * idf)

        tf_idf_matrix.append(tf_idf_row)

    return tf_idf_matrix


def normalize_data(mails: List[List[float]]) -> List[List[float]]:
    return sklearn.preprocessing.normalize(mails)


def centroid(mails: List[List[float]]) -> List[float]:
    centroid_vector = []

    for _ in mails[0]:
        centroid_vector.append(0)

    for mail in mails:
        for index, feature in enumerate(mail):
            centroid_vector[index] += feature

    for index, _ in enumerate(centroid_vector):
        centroid_vector[index] = centroid_vector[index] / len(mails)

    return centroid_vector

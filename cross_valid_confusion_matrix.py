import sklearn.metrics
import utils

cross_res = []

training_vectors, target_values = utils.get_data()

result_confusion_matrix = {}

for type_name, model in utils.MODEL_MAP.items():

    predicted_spam = sklearn.model_selection.cross_val_predict(model, training_vectors, target_values, cv=10)

    result_confusion_matrix[type_name] = sklearn.metrics.confusion_matrix(
        target_values, predicted_spam
    )  # https://scikit-learn.org/stable/modules/generated/sklearn.metrics.confusion_matrix.html


for type_name, confusion_matrix in result_confusion_matrix.items():
    print(type_name)

    print(f"\t\tHAM\t|\tSPAM")
    row_one = confusion_matrix[0]
    row_two = confusion_matrix[1]
    print(f"\tHAM|\t{row_one[0]}\t|\t{row_one[1]}")
    print(f"\tSPAM|\t{row_two[0]}\t|\t{row_two[1]}")
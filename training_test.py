import sklearn.metrics
import time
import utils

# to plot stuff
import seaborn
import pandas
import matplotlib.pyplot

cross_res = []

training_vectors, target_values = utils.get_data()

training_features, test_features, training_spam, test_spam = sklearn.model_selection.train_test_split(
    training_vectors, target_values, random_state=42
)

result_confusion_matrix = {}

for type_name, model in utils.MODEL_MAP.items():
    trained_model = model.fit(training_features, training_spam)  # train

    predicted_spam = trained_model.predict(
        test_features
    )  # predict the spam from the test data

    result_confusion_matrix[type_name] = sklearn.metrics.confusion_matrix(
        test_spam, predicted_spam
    )  # https://scikit-learn.org/stable/modules/generated/sklearn.metrics.confusion_matrix.html

for type_name, confusion_matrix in result_confusion_matrix.items():
    print(type_name)

    print(f"\t\tHAM\t|\tSPAM")
    row_one = confusion_matrix[0]
    row_two = confusion_matrix[1]
    print(f"\tHAM|\t{row_one[0]}\t|\t{row_one[1]}")
    print(f"\tSPAM|\t{row_two[0]}\t|\t{row_two[1]}")

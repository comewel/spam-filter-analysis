import sklearn.decomposition
import sklearn.model_selection
import sklearn.svm

import utils

import numpy

cross_res = []

training_vectors, target_values = utils.get_data()

data_map = {
    "tf * 100": training_vectors,
    "tf_idf": utils.tf_idf_converter(training_vectors),
    "angular-data": utils.normalize_data(training_vectors),
}

result_normal_data = {}
for data_type, data in data_map.items():
    print(f"type: {data_type}")
    for type_name, model in utils.MODEL_MAP.items():
        res = sklearn.model_selection.cross_val_score(model, data, target_values, cv=10)
        print(f"\t{type_name} model -> ({res.mean()}, {res.var()})")
